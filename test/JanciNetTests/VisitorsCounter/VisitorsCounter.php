<?php
/**
 * This file is part of the VisitorsCounter.
 *
 * Copyright (c) 2015 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JanciNetTests\VisitorsCounter;

use Nette\Caching\Cache;
use Nette\Caching\Storages\MemoryStorage;
use Nette\Utils\DateTime;
use JanciNet\VisitorsCounter\VisitorsCounter;
use Tester;
use Tester\Assert;

$configuration = require_once __DIR__ . '/../bootstrap.php';

class VisitorsCounterTest extends Tester\TestCase
{
    /** @var string*/
    private $piwikCorrectToken;

    public function __construct($piwikCorrectToken) {
        $this->piwikCorrectToken = $piwikCorrectToken;
    }

    public function testGetCorrectData()
    {
        $token = $this->piwikCorrectToken;
        $visitorsCounter = new VisitorsCounter($token, 7, DateTime::from("2009-10-10"));
        $count = $visitorsCounter->getCurrentCount();
        Assert::notEqual(null, $count);
        Assert::notEqual(false, $count);
    }

    public function testInvalidToken()
    {
        $token = "john.doe.test";
        $visitorsCounter = new VisitorsCounter($token, 7, DateTime::from("2015-10-10"));
        $count = $visitorsCounter->getCurrentCount();
        Assert::null(null, $count);
    }

    public function testGetDataFromCache() {
        $token = $this->piwikCorrectToken;
        $storage = new MemoryStorage();
        $cache = new Cache($storage);
        $visitorsCounter = new VisitorsCounter($token, 7, DateTime::from("2009-10-10"));
        $visitorsCounter->setCache($cache);
        $visitorsCounter->getCurrentCount();

        $start = microtime(true);
        $visitorsCounter->getCurrentCount();
        $elapsedTime = (microtime(true) - $start);
        echo "Elapsed time: " . $elapsedTime . "\n";
        Assert::true($elapsedTime < 0.01);
    }
}

\run(new VisitorsCounterTest($configuration["piwik.token"]));
