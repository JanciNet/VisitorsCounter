<?php
/**
 * This file is part of the VisitorCounter.
 *
 * Copyright (c) 2015 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JanciNet\VisitorsCounter;

use Nette\Caching\Cache;

class VisitorsCounter
{
    /** @var string */
    private $token;

    /** @var string */
    private $protocol = "http";

    /** @var string */
    private $server = "piwik.janci.net";

    /** @var \DateTime */
    private $initDay;

    /** @var int */
    private $siteId;

    /** @var Cache */
    private $cache;

    /** @var string */
    private $cacheKey = "visitors-counter";

    /**
     * @param \DateTime $initDay
     * @param string $token
     * @param int $siteId
     */
    public function __construct($token, $siteId, \DateTime $initDay) {
        $this->token = $token;
        $this->siteId = $siteId;
        $this->initDay = $initDay;
    }

    public function setCache(Cache $cache) {
        $this->cache = $cache;
    }

    public function getCurrentCount()
    {
        if (isset($this->cache)) {
            $value = $this->cache->load($this->cacheKey);

            return (isset($value)) ?  $value : $this->getCurrentCountWithRequest();
        } else {
            return $this->getCurrentCountWithRequest();
        }
    }

    /**
     * Gets visitors count from Piwik API.
     * Example for request to api:
     * http://piwik.janci.net/index.php?module=API&method=VisitsSummary.getVisits&idSite=7&period=year&date=today&format=PHP&prettyDisplay=true&token_auth=8e41f34cc4db3c12345150a5d35f34a5
     *
     * @return int|null
     */
    public function getCurrentCountWithRequest() {
        /**  Statistiky **/
        $token = $this->token;
        $serverUrl = $this->server;
        $serverProtocol = $this->protocol;
        $serverMethod = 'VisitsSummary.getVisits';
        $period = "range";
        $date = date("Y-m-d", $this->initDay->getTimestamp()) . "," . date("Y-m-d");

        $statistics_url  = $serverProtocol.'://'.$serverUrl.'/?module=API&method='.$serverMethod;
        $statistics_url .='&idSite='.$this->siteId.'&period='.$period.'&date='.$date.'&format=PHP&prettyDisplay=true&token_auth='.$token;

        $httpContent = @file_get_contents($statistics_url);
        if($httpContent === false) {
            return null;
        }

        $data = unserialize($httpContent);
        if (!is_double($data) && !is_int($data)) {
            return null;
        }

        if (isset($this->cache)) {
            $this->cache->save($this->cacheKey, $data, array(
                Cache::EXPIRE => '5 minutes'
            ));
        }

        return $data;
    }
}
